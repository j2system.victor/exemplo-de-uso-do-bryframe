var BryPDFEditorClass;
var PDFEditorScript = {};
(async function () {
  /** Espera pelo bry-es-script.js ser carregado. */
  while (!BryPDFEditorClass) {
    await new Promise((resolve) => setTimeout(resolve, 0));
  }

  /** Cria um módulo de comunicação informando a tag iframe com o qual irá se comunicar. */
  var BryPDFEditor = new BryPDFEditorClass(
    document.getElementById("bry-es-frame")
  );

  /**
   * Callback do evento onFinish acionado quando o usuário clica em finalizar no iframe PDFEditor.
   * @param {Object} param
   * @param {Participant[]} param.participants - Lista de assinantes do documento
   */
  BryPDFEditor.onFinish = ({ participants }) => {
    $("#json-positions-valor").html(
      JSON.stringify({ participants }, null, 2)
    );
  }

  /**
   * Insere um JSON de exemplo na página HTML para manipulação e 
   * posteriormente ser repassado ao iframe pelo BryPDFEditor.configurePDFEditor()
   */
  (() => {
    let params = {
      participants: [
        {
          name: "Jefferson Bastos",
          email: "exemplo@mail.com",
          config: [
            { x: 0, y: 0, pages: "1", scale: 1, height: 20, width: 60 },
            { x: 75, y: 25, pages: "1", scale: 1, height: 20, width: 60},
          ],
        },
        {
          name: "César Corrêa",
          email: "exemplo@mail.com",
          config: [
            { x: 75, y: 0, pages: "1", scale: 1 },
            { x: 0, y: 25, pages: "1", scale: 1 },
          ],
        },
      ],
    };
    $("#json-entrada-valor").html(JSON.stringify(params, null, 2));
  })();

  /**
   * Responsável por repassar os parâmetros de configuração para o iframe.
   * Idealmente estaria atrelada ao evento onload do iframe.
   */
  PDFEditorScript.configurePDFEditor = function () {
    const multipart = new FormData(document.getElementById("dados-coleta"));
    const file = multipart.get("gedocs");
    if (file.size == 0) {
      return alert("Selecione um documento no passo 1.");
    }
    let params;
    try {
      const json = $("#json-entrada-valor").html().replaceAll("<br>", "\n");
      params = JSON.parse(json);
    } catch (err) {
      return alert("JSON inválido");
    }
    BryPDFEditor.configurePDFEditor({
      document: file,
      ...params,
    })
  };
})();
