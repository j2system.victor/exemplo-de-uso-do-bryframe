const express = require("express");
const cors = require("cors");

const app = express();

app.use(cors());
app.use(express.static("static"));

app.listen(8082, () => {
  console.log("Client Running on 8082");
});
